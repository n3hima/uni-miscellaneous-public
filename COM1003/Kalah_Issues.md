Issues with the Kalah specification and associated files
========================================================

- In KalahGameState, the parameters listed in the JavaDoc for getKalah do not match the parameters specified in the actual code.
- In the specification, under part 1 of Step 2, reference is made to black and white counters. No difference in the colours of the counters was specified in the game outline, and all the counters used in the demontration in the lecture were white. What do the black and white counters signify?
- In KalahGameState, the number of side pits is specified as a constant, however in the JavaDoc for methods such as getNumStones(), the site pit number is specified as being explicitly between 1 and 6. Are we to throw an exception if the number is greater than 6, or greater than KalahGameState.NUM_SIDE_PITS?