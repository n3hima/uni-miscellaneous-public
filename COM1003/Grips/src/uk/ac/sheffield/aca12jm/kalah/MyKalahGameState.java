package uk.ac.sheffield.aca12jm.kalah;

import uk.ac.sheffield.com1003.kalah.EmptySidePitException;
import uk.ac.sheffield.com1003.kalah.IllegalSidePitNumberException;
import uk.ac.sheffield.com1003.kalah.InvalidStartingStonesException;
import uk.ac.sheffield.com1003.kalah.KalahGameState;
import uk.ac.sheffield.com1003.kalah.Player;

public class MyKalahGameState extends KalahGameState {
	private Player playerA, playerB;
	
	// True means Player A's turn
	private Player turn;
	
	// 0 = A's Kalah, 1--KalahGameState.NUM_SIDE_PITS are B's side pits,
	// 1+KalahGameState.NUM_SIDE_PITS = B's Kalah, etc.
	private int[] pits;
	private static final int A_KALAH_INDEX = 0;
	private static final int B_KALAH_INDEX = 1+KalahGameState.NUM_SIDE_PITS;
	private Player winner;
	
	private static boolean isPlayerA(int index) {
		return index > B_KALAH_INDEX || index == A_KALAH_INDEX;
	}
	
	private static boolean isPlayerB(int index) {
		return index > A_KALAH_INDEX && index <= B_KALAH_INDEX;
	}
	
	private static boolean isKalah(int index) {
		return index == A_KALAH_INDEX || index == B_KALAH_INDEX;
	}

	public void startGame(Player playerA, Player playerB, int startingStones)
			throws InvalidStartingStonesException {
		if (startingStones < 1)
			throw new InvalidStartingStonesException();
		
		this.playerA = playerA;
		this.playerB = playerB;
		this.turn = playerA;
		
		this.pits = new int[2*(1 + KalahGameState.NUM_SIDE_PITS)];
		for (int i = 0; i < pits.length; i++) {
			if (!(i == A_KALAH_INDEX || i == B_KALAH_INDEX))
				pits[i] = startingStones;
		}
	}

	public Player getPlayerA() {
		return playerA;
	}

	public Player getPlayerB() {
		return playerB;
	}

	public Player getTurn() {
		return turn;
	}

	public int getKalah(Player player) {
		if (player == playerA) {
			return pits[A_KALAH_INDEX];
		} else if (player == playerB) {
			return pits[B_KALAH_INDEX];
		} else return -1;
	}

	public int getNumStones(int sidePitNum)
			throws IllegalSidePitNumberException {
		if (sidePitNum < 1 || sidePitNum > KalahGameState.NUM_SIDE_PITS)
			throw new IllegalSidePitNumberException();
		
		return getNumStones(turn, sidePitNum);
	}

	public int getNumStones(Player player, int sidePitNum)
			throws IllegalSidePitNumberException {
		if (sidePitNum < 1 || sidePitNum > KalahGameState.NUM_SIDE_PITS)
			throw new IllegalSidePitNumberException();
		
		if (player == playerA) {
			return pits[B_KALAH_INDEX + sidePitNum];
		} else if (player == playerB) {
			return pits[A_KALAH_INDEX + sidePitNum];
		} else return -1;
	}

	public int getScore(Player player) {
		if (player != playerA && player != playerB) {
			return -1;
		} else {
			int total = 0;
			for (int i = 1; i <= KalahGameState.NUM_SIDE_PITS; i++)
				total += getNumStones(player, i);
			total += getKalah(player);
			return total;
		}
	}

	public void makeMove(int sidePitNum) throws IllegalSidePitNumberException,
			EmptySidePitException {
		if (sidePitNum < 1 || sidePitNum > KalahGameState.NUM_SIDE_PITS)
			throw new IllegalSidePitNumberException();
		
		int startIndex;
		if (turn == playerA)
			startIndex = B_KALAH_INDEX + sidePitNum;
		else
			startIndex = A_KALAH_INDEX + sidePitNum;
		
		if (pits[startIndex] == 0)
			throw new EmptySidePitException(sidePitNum);
		
		// "Pick up" the stones in the current pit.
		int stonesInPlay = pits[startIndex];
		pits[startIndex] = 0;
		
		int finalPosition = -1;
		// Add a stone to each subsequent pit, skipping any opponent's Kalahs we might encounter.
		for (int i = startIndex + 1; stonesInPlay > 0; i++) {
			// Skip over the opponent's Kalah.
			if (!(	turn == playerA && i % pits.length == B_KALAH_INDEX ||
					turn == playerB && i % pits.length == A_KALAH_INDEX)) {
				pits[i % pits.length]++;
				stonesInPlay--;
				
				finalPosition = i % pits.length;
			}
		}
		
		boolean landedInEmptySidePit = pits[finalPosition] == 1 && finalPosition != A_KALAH_INDEX && finalPosition != B_KALAH_INDEX;
		boolean landedInOwnSidePit = (turn == playerA && isPlayerA(finalPosition)) || (turn == playerB && isPlayerB(finalPosition));
		
		if (landedInEmptySidePit && landedInOwnSidePit) {
			int kalah = turn == playerA ? A_KALAH_INDEX : B_KALAH_INDEX;
			
			int oppositePit = pits.length - finalPosition;
			pits[kalah] += pits[oppositePit] + 1;
			pits[oppositePit] = 0;
			pits[finalPosition] = 0;
		}
		
		// Only change the turn if we didn't land in a Kalah. (Note we cannot land in the opponent's Kalah, so we don't bother checking for that.)
		if (!isKalah(finalPosition))
			turn = turn == playerA ? playerB : playerA;
	}

	public boolean isGameOver() {
		boolean over = true;
		for (int i = 1; i <= KalahGameState.NUM_SIDE_PITS; i++) {
			over = over && getNumStones(turn, i) == 0;
		}
		if (over)
			winner = turn == playerA ? playerB : playerA;
		return over;
	}

	public Player getWinner() {
		isGameOver();
		return winner;
	}

	public KalahGameState copy() {
		MyKalahGameState newState = new MyKalahGameState();
		newState.playerA = playerA;
		newState.playerB = playerB;
		newState.pits = new int[pits.length];
		for (int i = 0; i < pits.length; i++) {
			newState.pits[i] = pits[i];
		}
		newState.turn = turn;
		newState.winner = winner;
		
		return newState;
	}

}
