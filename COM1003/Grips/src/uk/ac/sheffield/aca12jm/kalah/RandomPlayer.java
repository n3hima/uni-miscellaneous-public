package uk.ac.sheffield.aca12jm.kalah;

import uk.ac.sheffield.com1003.kalah.KalahGameState;
import uk.ac.sheffield.com1003.kalah.NoMoveAvailableException;
import uk.ac.sheffield.com1003.kalah.Player;

public class RandomPlayer extends Player {

	public RandomPlayer(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int chooseMove(KalahGameState gameState)
			throws NoMoveAvailableException {
		boolean atLeastOnePitHasStones = false;
		for (int i=1; i <= KalahGameState.NUM_SIDE_PITS; i++) {
			if (gameState.getNumStones(i) > 0) {
				atLeastOnePitHasStones = true;
			}
		}
		if (!atLeastOnePitHasStones) {
			throw new NoMoveAvailableException();
		}
		return 1 + (int)(Math.random() * KalahGameState.NUM_SIDE_PITS);
	}

}
