package uk.ac.sheffield.aca12jm.kalah;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import uk.ac.sheffield.com1003.kalah.*;

public class KalahGameStateTest {
	private KalahGameState gameState;
	private Player playerA;
	private Player playerB;
	private final int NUM_STONES = 3;

	@Before
	public void setUp() throws Exception {
		gameState = new MyKalahGameState();
		playerA = new KeyboardPlayer("Player A");
		playerB = new KeyboardPlayer("Player B");
		gameState.startGame(playerA, playerB, NUM_STONES);
	}

	@Test
	public void testStartGame() {
		KalahGameState blankGameState = new MyKalahGameState();
		blankGameState.startGame(playerA, playerB, NUM_STONES);
		assertEquals(blankGameState.getPlayerA(), playerA);
		assertEquals(blankGameState.getPlayerB(), playerB);
		
		for (int i = 1; i <= KalahGameState.NUM_SIDE_PITS; i++) {
			assertEquals(blankGameState.getNumStones(playerA, i), NUM_STONES);
			assertEquals(blankGameState.getNumStones(playerB, i), NUM_STONES);
			
		}
	}
	
	@Test(expected=InvalidStartingStonesException.class)
	public void testInvalidStartingStonesException() {
		KalahGameState blankGameState = new MyKalahGameState();
		blankGameState.startGame(playerA, playerB, 0);
	}

	@Test
	public void testGetTurn() throws IllegalSidePitNumberException, EmptySidePitException {
		assertEquals(gameState.getTurn(), playerA);
		// Final stone should land in A's Kalah, so they repeat their go.
		gameState.makeMove(4);
		
		assertEquals(gameState.getTurn(), playerA);
		gameState.makeMove(1);
		assertEquals(gameState.getTurn(), playerB);
		gameState.makeMove(1);
		assertEquals(gameState.getTurn(), playerA);
	}

	@Test
	public void testGetKalah() throws IllegalSidePitNumberException, EmptySidePitException {
		assertTrue(gameState.getKalah(playerA) == 0);
		gameState.makeMove(6);
		assertTrue(gameState.getKalah(playerA) == 1);
		assertTrue(gameState.getKalah(playerB) == 0);
		gameState.makeMove(6);
		assertTrue(gameState.getKalah(playerB) == 1);
	}

	@Test
	public void testGetNumStonesInt() throws IllegalSidePitNumberException, EmptySidePitException {
		assertTrue(gameState.getNumStones(6) == 3);
		gameState.makeMove(6);
		assertTrue(gameState.getNumStones(1) == 4);
		gameState.makeMove(6);
		assertTrue(gameState.getNumStones(6) == 0);
	}
	
	@Test(expected=IllegalSidePitNumberException.class)
	public void testGetNumStonesIntException() throws EmptySidePitException {
		gameState.makeMove(0);
	}

	@Test
	public void testGetNumStonesPlayerInt() throws IllegalSidePitNumberException, EmptySidePitException {
		assertTrue(gameState.getNumStones(playerA, 6) == 3);
		assertTrue(gameState.getNumStones(playerB, 6) == 3);
		gameState.makeMove(6);
		assertTrue(gameState.getNumStones(playerB, 1) == 4);
		assertTrue(gameState.getNumStones(playerA, 6) == 0);
		gameState.makeMove(5);
		assertTrue(gameState.getNumStones(playerB, 6) == 4);
		assertTrue(gameState.getNumStones(playerA, 1) == 4);
	}
	
	@Test(expected=IllegalSidePitNumberException.class)
	public void testGetNumStonesPlayerIntException() throws EmptySidePitException {
		gameState.makeMove(KalahGameState.NUM_SIDE_PITS + 1);
	}

	@Test
	public void testGetScore() throws IllegalSidePitNumberException, EmptySidePitException {
		gameState.makeMove(2);
		gameState.makeMove(4);
		gameState.makeMove(1);
		gameState.makeMove(6);
		gameState.makeMove(3);
		
		assertTrue(gameState.getScore(playerA) == 12);
		assertTrue(gameState.getScore(playerB) == 24);
		
		assertTrue(gameState.getScore(new KeyboardPlayer("")) == -1);
	}

	@Test
	public void testMakeMove() throws IllegalSidePitNumberException, EmptySidePitException {
		assertTrue(gameState.getTurn() == playerA);
		gameState.makeMove(4);
		assertTrue(gameState.getTurn() == playerA);
		
		// A's turn, final stone should land in 4, which is empty. A takes stones in B's 2.
		gameState.makeMove(1);
		assertTrue(gameState.getKalah(playerA) == 5);
		
		gameState.makeMove(2);
		assertTrue(gameState.getNumStones(playerB, 3) == 1);
		assertTrue(gameState.getNumStones(playerB, 4) == 4);
		assertTrue(gameState.getNumStones(playerB, 5) == 4);
		
		// Test skipping over opponent's Kalah
		gameState.makeMove(2);
		gameState.makeMove(4);
		gameState.makeMove(3);
		gameState.makeMove(5);
		gameState.makeMove(3);
		gameState.makeMove(3);
		gameState.makeMove(5);
		gameState.makeMove(1);
		gameState.makeMove(4);
		gameState.makeMove(6);
		assertTrue(gameState.getNumStones(playerA, 1) == 3);
		
	}
	
	@Test(expected=IllegalSidePitNumberException.class)
	public void testMakeMoveBadSidePit() throws EmptySidePitException {
		gameState.makeMove(0);
	}
	
	@Test(expected=IllegalSidePitNumberException.class)
	public void testMakeMoveBadSidePit2() throws EmptySidePitException {
		gameState.makeMove(7);
	}
	
	@Test
	public void testMakeMoveEmptySidePit() throws IllegalSidePitNumberException {
		// For some reason this doesn't work if I use the @Test(expected=...) method as above.
		try {
			gameState.makeMove(4); // Repeated go.
			gameState.makeMove(4);
			fail("No exception was thrown.");
		} catch (EmptySidePitException e) {
			// Success
		}
	}

	@Test
	public void testIsGameOver() throws IllegalSidePitNumberException, EmptySidePitException {
		gameState.makeMove(4);
		gameState.makeMove(1);
		gameState.makeMove(4);
		gameState.makeMove(1);
		gameState.makeMove(5);
		gameState.makeMove(2);
		gameState.makeMove(5);
		gameState.makeMove(2);
		gameState.makeMove(1);
		gameState.makeMove(1);
		gameState.makeMove(3);
		gameState.makeMove(4);
		gameState.makeMove(6);
		gameState.makeMove(1);
		gameState.makeMove(4);
		gameState.makeMove(2);
		gameState.makeMove(5);
		assertFalse(gameState.isGameOver());
		gameState.makeMove(5);
		assertTrue(gameState.isGameOver());
		
	}

	@Test
	public void testGetWinner() throws IllegalSidePitNumberException, EmptySidePitException {
		assertTrue(gameState.getWinner() == null);
		gameState.makeMove(4);
		gameState.makeMove(1);
		gameState.makeMove(4);
		gameState.makeMove(1);
		gameState.makeMove(5);
		gameState.makeMove(2);
		gameState.makeMove(5);
		gameState.makeMove(2);
		gameState.makeMove(1);
		gameState.makeMove(1);
		gameState.makeMove(3);
		gameState.makeMove(4);
		gameState.makeMove(6);
		gameState.makeMove(1);
		gameState.makeMove(4);
		gameState.makeMove(2);
		gameState.makeMove(5);
		assertTrue(gameState.getWinner() == null);
		gameState.makeMove(5);
		assertTrue(gameState.getWinner() == playerB);
	}

	@Test
	public void testCopy() throws IllegalSidePitNumberException, EmptySidePitException {
		KalahGameState newState = gameState.copy();
		assertTrue(newState != gameState);
		assertTrue(gameState.getPlayerA() == newState.getPlayerA());
		assertTrue(gameState.getPlayerB() == newState.getPlayerB());
		for (int i = 1; i < KalahGameState.NUM_SIDE_PITS; i++) {
			assertTrue(gameState.getNumStones(playerA, i) == newState.getNumStones(playerA, i));
			assertTrue(gameState.getNumStones(playerB, i) == newState.getNumStones(playerB, i));
		}
		assertTrue(gameState.getKalah(playerA) == newState.getKalah(playerB));
		
		newState.makeMove(4);
		assertTrue(gameState.getNumStones(playerA, 4) != newState.getNumStones(playerA, 4));
	}

}
