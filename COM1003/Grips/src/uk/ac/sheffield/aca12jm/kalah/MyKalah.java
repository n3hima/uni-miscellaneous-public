package uk.ac.sheffield.aca12jm.kalah;

import uk.ac.sheffield.com1003.kalah.ConsoleDisplay;
import uk.ac.sheffield.com1003.kalah.Kalah;
import uk.ac.sheffield.com1003.kalah.KeyboardPlayer;

public class MyKalah {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Kalah().play(new MyKalahGameState(), new RandomPlayer("Random AI"), new KeyboardPlayer("Human"), 3, new ConsoleDisplay());
	}

}
