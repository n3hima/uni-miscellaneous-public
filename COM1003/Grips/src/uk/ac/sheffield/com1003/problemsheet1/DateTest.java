package uk.ac.sheffield.com1003.problemsheet1;

import static org.junit.Assert.*;

import org.junit.*;

public class DateTest {
	
	Date date1, date2, date3, date4;

	@Before
	public void setUp() throws Exception {
		date1 = new Date(6, 2, 2013);
		date2 = new Date(8, 2, 2013);
		date3 = new Date(6, 3, 2013);
		date4 = new Date(6, 2, 2014);
	}

	@Test
	public void testConstructorAndSetters() {
		assertTrue(date1.getDay() == 6);
		assertTrue(date1.getMonth() == 2);
		assertTrue(date1.getYear() == 2013);
		
		Date date5 = new Date(1, 2, 2013);
		date5.setDay(-1);
		assertTrue(date5.getDay() == 1);
		date5.setDay(31);
		assertTrue(date5.getDay() == 28);
		
		date5.setMonth(-1);
		assertTrue(date5.getMonth() == 1);
		date5.setMonth(13);
		assertTrue(date5.getMonth() == 12);
	}
	
	@Test
	public void testNumberOfDaysInMonth() {
		for(int i = 1; i <= 12; i++) {
			int days = Date.numberOfDaysInMonth(i, 2013);
			assertTrue(days == 28 || days == 30 || days == 31);
		}
	}
	
	@Test
	public void testDaysBetween() {
		assertTrue(date1.daysBetween(date2) == 2);
		assertTrue(date1.daysBetween(date3) == 28);
		assertTrue(date1.daysBetween(date4) == 365);
		assertTrue(date3.daysBetween(date4) == 365-28);
		
		Date date5 = new Date(31, 12, -1);
		Date date6 = new Date(1, 1, 1);
		assertTrue(date5.daysBetween(date6) == 1);
	}
	
	@Test
	public void testBefore() {
		assertTrue(date1.before(date2));
		assertTrue(date1.before(date4));
		assertTrue(!date3.before(date1));
	}
	
	@Test
	public void testToString() {
		assertTrue(date1.toString().equals("6/2/2013"));
		assertTrue(date2.toString().equals("8/2/2013"));
		assertTrue(date3.toString().equals("6/3/2013"));
	}
	
	@Test
	public void isLeapYear() {
		assertTrue(Date.isLeapYear(2012));
		assertTrue(Date.isLeapYear(2008));
		assertTrue(Date.isLeapYear(2000));
		assertTrue(Date.isLeapYear(1600));
		
		assertTrue(!Date.isLeapYear(2013));
		assertTrue(!Date.isLeapYear(2015));
		assertTrue(!Date.isLeapYear(2100));
		assertTrue(!Date.isLeapYear(1900));
	}
}
