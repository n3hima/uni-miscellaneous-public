Problem Sheet 1: Report
=======================

Bug 1
-----
Location: Constructor.

The setDay() method was being called before setMonth(), which resulted in a
failure.

Fix: Swapped the calls to setDay() and setMonth().

Bug 2
-----
Location: daysBetween().

When getting the days between dates in different months of the same year, the
number of days in the final month is unnecessarily added.

Fix: Changed the <= operator to a < operator on line 51.

Bug 3
-----
Location: daysBetween().

When comparing a BC date to an AD date, the days in the year 0 are counted,
however the progression of years goes from 1 BC to 1 AD with no year 0.

Fix: When adding the number of days in the year, we check for the year 0 and
skip over it.
