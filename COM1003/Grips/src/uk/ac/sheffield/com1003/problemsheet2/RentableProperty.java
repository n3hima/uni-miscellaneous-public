package uk.ac.sheffield.com1003.problemsheet2;

public class RentableProperty extends Property {
	private int rentPrice;
	
	public RentableProperty(int number, String street, int rentPrice) {
		super(number, street);
		this.rentPrice = rentPrice;
	}

	@Override
	public String getInfo() {
		return super.getInfo() + "rent at " + rentPrice + " pounds per month.";
	}

}
