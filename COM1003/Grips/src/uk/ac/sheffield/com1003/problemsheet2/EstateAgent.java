package uk.ac.sheffield.com1003.problemsheet2;

import java.util.ArrayList;

public class EstateAgent {
	private ArrayList<Property> properties;
	
	public EstateAgent() {
		properties = new ArrayList<Property>();
	}
	
	public void addProperty(Property property) {
		properties.add(property);
	}
	
	public String getInfoForAllProperties() {
		String outStr = "";
		for (Property p : properties) {
			outStr += p.getInfo() + "\n";
		}
		return outStr;
	}
}
