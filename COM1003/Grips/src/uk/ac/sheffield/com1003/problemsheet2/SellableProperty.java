package uk.ac.sheffield.com1003.problemsheet2;

public class SellableProperty extends Property {
	private int salePrice;
	
	public SellableProperty(int number, String street, int salePrice) {
		super(number, street);
		this.salePrice = salePrice;
	}

	@Override
	public String getInfo() {
		return number + " " + street + ", for sale at " + salePrice + " pounds.";
	}

}
