package uk.ac.sheffield.com1003.problemsheet2;

public abstract class Property {
	protected int number;
	protected String street;
	
	public Property(int number, String street) {
		this.number = number;
		this.street = street;
	}
	
	public String getInfo() {
		return number + " " + street + ", for ";
	}
}
