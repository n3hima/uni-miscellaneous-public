module Search
    ( dfSearch
    , bfSearch
    , ExpandF
    , Predicate
    ) where

import Data.List (nub, (\\))
import Debug.Trace

type ExpandF a = a -> [a]
type Predicate a = a -> Bool
type OpenF a = [a] -> [a] -> [a]

newtype SearchNode a = SearchNode {getPath :: [a]}
instance Eq a => Eq (SearchNode a) where
    (==) (SearchNode a) (SearchNode b) = head a == head b
instance Show a => Show (SearchNode a)

getNode :: SearchNode a -> a
getNode = head . getPath

prepend :: a -> SearchNode a -> SearchNode a
prepend x (SearchNode xs) = SearchNode (x:xs)

concatSN :: SearchNode a -> SearchNode a -> SearchNode a
concatSN (SearchNode a) (SearchNode b) = SearchNode $ a ++ b

dfSearch :: (Show a, Eq a) => ExpandF a -> Predicate a -> a -> [a]
dfSearch expand goalP startNode = getPath $ search' dfOpenF expand goalP [SearchNode [startNode]] []

bfSearch :: (Show a, Eq a) => ExpandF a -> Predicate a -> a -> [a]
bfSearch expand goalP startNode = getPath $ search' bfOpenF expand goalP [SearchNode [startNode]] []

search' :: (Show a, Eq a) => OpenF (SearchNode a) -> ExpandF a -> Predicate a -> [SearchNode a] -> [SearchNode a] -> SearchNode a
search' newOpenF expand goalP (currentSNode:open) closed = 
    if goalP $ getNode currentSNode
        then currentSNode
        else search' newOpenF expand goalP newOpen (currentSNode:closed)
    where
        newOpen = newOpenF unexploredNodes open
        unexploredNodes = nub $ ((expandPath expand currentSNode) \\ badList)
        badList = (currentSNode:open) ++ closed

dfOpenF :: OpenF a
dfOpenF unexplored open = unexplored ++ open
bfOpenF :: OpenF a
bfOpenF unexplored open = open ++ unexplored

expandPath :: ExpandF a -> SearchNode a -> [SearchNode a]
expandPath expand path = map (flip prepend path) $ expand $ getNode path
