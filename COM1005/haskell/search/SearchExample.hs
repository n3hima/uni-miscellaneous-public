import Search

import Data.List ((\\))

maxA = 4
maxB = 3
trgt = 2

data Jugs = Jugs {getA :: Int, getB :: Int} deriving (Show, Eq)
pr :: Predicate Jugs
pr (Jugs a b) = (a == trgt || b == trgt)

expand :: ExpandF Jugs
expand jugs = empties jugs ++ into jugs ++ fills jugs

empties :: ExpandF Jugs
empties (Jugs a b) | a == 0 && b == 0 = []
                   | a == 0 || b == 0 = [Jugs 0 0]
                   | otherwise        = [Jugs a 0, Jugs 0 b]

into (Jugs a b) | a == 0 && b == 0 = []
                | a == 0           = [ bIntoA (Jugs a b) ]
                | b == 0           = [ aIntoB (Jugs a b) ]
                | otherwise        = [ aIntoB (Jugs a b)
                                     , bIntoA (Jugs a b)
                                     ]

fills (Jugs a b) = [Jugs a maxB, Jugs maxA b]

aIntoB :: Jugs -> Jugs
aIntoB (Jugs a b) | b == maxB = Jugs a maxB
                  | a == 0    = Jugs 0 b
                  | otherwise = aIntoB $ Jugs (a-1) (b+1)

bIntoA :: Jugs -> Jugs
bIntoA (Jugs a b) | a == maxA = Jugs maxA b
                  | b == 0    = Jugs a 0
                  | otherwise = bIntoA $ Jugs (a+1) (b-1)

test sF = map show $ reverse $ sF expand pr $ Jugs 0 0

main = do
    mapM_ putStrLn $ test bfSearch
